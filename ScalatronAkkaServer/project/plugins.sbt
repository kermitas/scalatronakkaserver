resolvers += Resolver.url( "sbt-plugin-releases" , new URL("http://scalasbt.artifactoryonline.com/scalasbt/sbt-plugin-releases/") )(Resolver.ivyStylePatterns)

// --- one-jar
//addSbtPlugin( "com.github.retronym" % "sbt-onejar" % "0.8" )

// --- sbt-assembly
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.8.7")
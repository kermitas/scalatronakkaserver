package main.scala.org.kermitas.scalatron
{
  import _root_.akka.actor._
  import java.util.logging._

  object ScalatronAkkaServer
  {
    protected val log = java.util.logging.Logger.getLogger( getClass.getName )

    def main(args: Array[String]) =
    {
      log.info("Welcome in " + getClass.getName + ", current time is " + System.currentTimeMillis + " ms")

      // ----

      log.fine( "Loading configuration" )
      val typesafeConfig = com.typesafe.config.ConfigFactory.load

      // ----

      val scalatronAkkaServerConfig = server.akka.ScalatronAkkaServerConfig( typesafeConfig.getConfig( classOf[ server.akka.ScalatronAkkaServer ].getSimpleName ) )

      // ----

      configureLogging( scalatronAkkaServerConfig.logLevel , "main.scala" )

      // ----

      log.fine( "Starting actor system '" + scalatronAkkaServerConfig.actorSystemName + "'" )
      val actorSystem = ActorSystem( scalatronAkkaServerConfig.actorSystemName , typesafeConfig )

      // ----

      if( !scalatronAkkaServerConfig.workAsAkkaRemoteDeploymentNode )
      {
        val startMessage = server.akka.ScalatronAkkaServer.Start( scalatronAkkaServerConfig )
        val mainActor = actorSystem.actorOf( Props[ server.akka.ScalatronAkkaServer ] , name = classOf[ server.akka.ScalatronAkkaServer ].getSimpleName )

        sendStartMessageAndAwaitForStartConfirmationOrShutdown( actorSystem , mainActor , startMessage , scalatronAkkaServerConfig.mainActorExecuteInitialMessageTimeoutInSeconds )
      }
      else
      {
        log.info( "Working as akka remote deployment node, press ctrl+c to exit..." )
      }

      // ----

    }

    protected def sendStartMessageAndAwaitForStartConfirmationOrShutdown( actorSystem : ActorSystem , mainActor : ActorRef , startMessage : AnyRef , timeoutInSeconds : Int )
    {
      import scala.concurrent.duration._
      import _root_.akka.util.Timeout
      import _root_.akka.pattern.ask
      implicit val waitForResponseMessageTimeout = Timeout(timeoutInSeconds seconds)

      log.fine( "Sending initial message " + startMessage )

      val startStatusFuture = mainActor ? startMessage

      implicit val dispatcherForFuture = actorSystem.dispatcher

      startStatusFuture.onFailure
      {
        case throwable : Throwable ⇒
        {
          performShutdown( actorSystem , mainActor , "Problem while waiting for main actor start confirmation" , throwable , 4 , false )
        }
      }

      startStatusFuture.onSuccess
      {
        case _root_.main.scala.org.kermitas.scalatron.server.akka.ScalatronAkkaServer.StartStatus( throwableOption ) ⇒
        {
          throwableOption match
          {
            case Some( throwable ) ⇒
            {
              performShutdown( actorSystem , mainActor , "Problem while executing main actor initial message" , throwable , 4 , false )
            }

            case None ⇒ log.info( "Main actor executed it's initial message, stopping 'JVM shutdown' watchdog" )
          }
        }
      }
    }

    protected def performShutdown( actorSystem : ActorSystem , mainActor : ActorRef , logMessage : String , throwable : Throwable , jvmStopDelayInSeconds : Int , trueIfSystemExitFalseIfRuntimeHalt : Boolean )
    {
      log.log( java.util.logging.Level.WARNING , logMessage , throwable )

      new _root_.main.scala.org.kermitas.scalatron.util.JVMKillingThread( jvmStopDelayInSeconds , trueIfSystemExitFalseIfRuntimeHalt ).start

      //actorSystem.stop( mainActor )
      actorSystem.shutdown
    }

    protected def configureLogging( logLevel : Level , mainLoggerName : String )
    {
      //val rootLogger = LogManager.getLogManager.getLogger( "" )
      val rootLogger = Logger.getLogger( mainLoggerName )
      rootLogger.setLevel( logLevel )

      if( logLevel != Level.OFF )
      {
        rootLogger.setUseParentHandlers( false )

        val consoleHanlder = new ConsoleHandler
        consoleHanlder.setLevel( logLevel )
        consoleHanlder.setFormatter( new _root_.main.scala.org.kermitas.logging.SimpleFormatter )

        rootLogger.addHandler( consoleHanlder )
      }
    }
  }
}
package main.scala.org.kermitas.scalatron.bot
{
  import akka.actor._
  import akka.event._
  import collection.immutable.List
  import main.scala.org.kermitas.scalatron.opcodes._


  // this is just a dummy bot class that allow us to write
  // val botActor = context.system.actorOf( Props[ main.scala.org.kermitas.scalatron.bot.Bot ].withDeploy(Deploy(scope = RemoteScope( akkaAddress ))) )
  // in BotService

  class Bot extends Actor
  {
    protected val log = Logging(context.system, this)

    def receive =
    {
      case incomingOpcodesAnonSeq : Seq[_] ⇒
      {
        val incomingOpcodesSeq = incomingOpcodesAnonSeq.asInstanceOf[ Seq[ IncomingOpcode ] ]

        log.warning( "This is only a dummy actor, should never receive any message!! But it received: " + incomingOpcodesSeq )
      }
    }
  }
}

package main.scala.org.kermitas.scalatron.server.akka
{
  import akka.actor._
  import akka.event._
  import akka.io._
  import akka.util._
  import scala.concurrent.duration._
  import main.scala.org.kermitas.scalatron.opcodes._

  object BotService
  {
    // ----

    sealed trait State
    case object Uninitialized extends State
    case object ReadingCommandLength extends State
    case object ReadingCommandBody extends State
    case object ParsingIncomingOpcodes extends State
    case object SerializingOutgoingOpcodes extends State
    case object SendingOutgoingOpcodes extends State

    // ----

    sealed trait StateData
      case object UninitializedStateData extends StateData
        class InitializedStateData( val botServiceConfig : BotServiceConfig , val akkaSocketActorManager : ActorRef , var incomingDataBuffer : ByteString , val remotelyDeployedBotActor : ActorRef , val incomingOpcodesParser : ActorRef , val outgoingOpcodesSerializer : ActorRef ) extends StateData
          class ReadingCommandLengthStateData( botServiceConfig : BotServiceConfig , akkaSocketActorManager : ActorRef , incomingDataBuffer : ByteString , remotelyDeployedBotActor : ActorRef , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef ) extends InitializedStateData( botServiceConfig , akkaSocketActorManager , incomingDataBuffer , remotelyDeployedBotActor , incomingOpcodesParser , outgoingOpcodesSerializer )
          class ReadingCommandBodyStateData( botServiceConfig : BotServiceConfig , akkaSocketActorManager : ActorRef , incomingDataBuffer : ByteString , remotelyDeployedBotActor : ActorRef , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef , val commandLength : Int ) extends InitializedStateData( botServiceConfig , akkaSocketActorManager , incomingDataBuffer , remotelyDeployedBotActor , incomingOpcodesParser , outgoingOpcodesSerializer )
          class ParsingIncomingOpcodesStateData( botServiceConfig : BotServiceConfig , akkaSocketActorManager : ActorRef , incomingDataBuffer : ByteString , remotelyDeployedBotActor : ActorRef , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef ) extends InitializedStateData( botServiceConfig , akkaSocketActorManager , incomingDataBuffer , remotelyDeployedBotActor , incomingOpcodesParser , outgoingOpcodesSerializer )
          class SerializingOutgoingOpcodesStateData( botServiceConfig : BotServiceConfig , akkaSocketActorManager : ActorRef , incomingDataBuffer : ByteString , remotelyDeployedBotActor : ActorRef , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef ) extends InitializedStateData( botServiceConfig , akkaSocketActorManager , incomingDataBuffer , remotelyDeployedBotActor , incomingOpcodesParser , outgoingOpcodesSerializer )
          class SendingOutgoingOpcodesStateData( botServiceConfig : BotServiceConfig , akkaSocketActorManager : ActorRef , incomingDataBuffer : ByteString , remotelyDeployedBotActor : ActorRef , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef ) extends InitializedStateData( botServiceConfig , akkaSocketActorManager , incomingDataBuffer , remotelyDeployedBotActor , incomingOpcodesParser , outgoingOpcodesSerializer )

    // ----

    sealed trait Messages

      sealed trait IncomingMessages extends Messages

        case class Init( botServiceConfig : BotServiceConfig , akkaSocketActorManager : ActorRef , incomingConnectionRemoteIpAddressAsString : String , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef ) extends IncomingMessages
        case class Stop( throwable : Throwable ) extends IncomingMessages

    // ----
  }

  class BotService extends Actor with FSM[BotService.State, BotService.StateData]
  {
    // TODO Configure: if any child actor will blow that should blow current actor too!!

    startWith(BotService.Uninitialized, BotService.UninitializedStateData)

    when( BotService.Uninitialized )
    {
      case Event( BotService.Init( botServiceConfig , akkaSocketActorManager , incomingConnectionRemoteIpAddressAsString , incomingOpcodesParser , outgoingOpcodesSerializer ) , BotService.UninitializedStateData ) ⇒
      {
        botServiceConfig.mappings.find( mapping => mapping.sourceIp.equals( incomingConnectionRemoteIpAddressAsString ) ) match
        {
          case Some( incomingConnectionSourceAddressToDestinationAkkaAddressMapping ) =>
          {
            log.debug( "Will use mapping: " + incomingConnectionRemoteIpAddressAsString + " <--> " + incomingConnectionSourceAddressToDestinationAkkaAddressMapping.destinationAkkaBotAddress )

            import akka.actor.{ Props, Deploy, Address, AddressFromURIString }
            import akka.remote.RemoteScope
            val akkaAddress = AddressFromURIString( incomingConnectionSourceAddressToDestinationAkkaAddressMapping.destinationAkkaBotAddress )
            val botActor = context.system.actorOf( Props[ main.scala.org.kermitas.scalatron.bot.Bot ].withDeploy(Deploy(scope = RemoteScope( akkaAddress ))) )

            setTimer( BotService.ReadingCommandLength.getClass.getSimpleName , StateTimeout , botServiceConfig.readingCommandLengthTimeoutInSeconds second , false )

            goto( BotService.ReadingCommandLength ) using new BotService.ReadingCommandLengthStateData( botServiceConfig , akkaSocketActorManager , ByteString( "" ) , botActor , incomingOpcodesParser , outgoingOpcodesSerializer )
          }

          case None => stop( FSM.Failure( new RuntimeException( "Could not find any address mapping " + incomingConnectionRemoteIpAddressAsString + " (incoming connection source IP) <--> ????? (akka address where Bot actor should be remotely deployed)" ) ) )
        }
      }
    }

    when( BotService.ReadingCommandLength )
    {
      case Event( StateTimeout , stateData ) ⇒
      {
        stop( FSM.Failure( new RuntimeException( "Timeout while waiting for incoming command length" ) ) )
      }

      case Event( Tcp.Received( incomingDataByteString ) , stateData : BotService.ReadingCommandLengthStateData ) ⇒
      {
        if( incomingDataByteString.length > 0 ) stateData.incomingDataBuffer = stateData.incomingDataBuffer ++ incomingDataByteString

        if ( stateData.incomingDataBuffer.size >= 4 )
        {
          cancelTimer( BotService.ReadingCommandLength.getClass.getSimpleName )

          val fourBytesByteString = stateData.incomingDataBuffer.take( 4 )
          stateData.incomingDataBuffer = stateData.incomingDataBuffer.drop( 4 )

          val commandLength = new java.io.DataInputStream( new java.io.ByteArrayInputStream( fourBytesByteString.toArray ) ).readInt()

          setTimer( BotService.ReadingCommandBody.getClass.getSimpleName , StateTimeout , stateData.botServiceConfig.readingCommandBodyTimeoutInSeconds second , false )

          gotoToReadCommandBodyState( new BotService.ReadingCommandBodyStateData( stateData.botServiceConfig , stateData.akkaSocketActorManager , stateData.incomingDataBuffer , stateData.remotelyDeployedBotActor , stateData.incomingOpcodesParser , stateData.outgoingOpcodesSerializer , commandLength ) )
        }
        else
        {
          stay using stateData
        }
      }
    }

    when( BotService.ReadingCommandBody )
    {
      case Event( StateTimeout , stateData ) ⇒
      {
        stop( FSM.Failure( new RuntimeException( "Timeout while waiting for incoming command body" ) ) )
      }

      case Event( Tcp.Received( incomingDataByteString ) , stateData : BotService.ReadingCommandBodyStateData ) ⇒
      {
        if( incomingDataByteString.length > 0 ) stateData.incomingDataBuffer = stateData.incomingDataBuffer ++ incomingDataByteString

        if ( stateData.incomingDataBuffer.size >= stateData.commandLength )
        {
          cancelTimer( BotService.ReadingCommandBody.getClass.getSimpleName )

          val incomingOpcodeAsByteString = stateData.incomingDataBuffer.take( stateData.commandLength )
          stateData.incomingDataBuffer = stateData.incomingDataBuffer.drop( stateData.commandLength )

          log.debug( "Received incoming opcode (ByteString) '" + incomingOpcodeAsByteString.utf8String + "'" )

          stateData.incomingOpcodesParser ! new IncomingOpcodesParser.Parse( incomingOpcodeAsByteString )

          setTimer( BotService.ParsingIncomingOpcodes.getClass.getSimpleName , StateTimeout , stateData.botServiceConfig.parsingIncomingOpcodesTimeoutInSeconds second , false )

          goto( BotService.ParsingIncomingOpcodes ) using new BotService.ParsingIncomingOpcodesStateData( stateData.botServiceConfig , stateData.akkaSocketActorManager , stateData.incomingDataBuffer , stateData.remotelyDeployedBotActor , stateData.incomingOpcodesParser , stateData.outgoingOpcodesSerializer )
        }
        else
        {
          stay using stateData
        }
      }
    }

    when( BotService.ParsingIncomingOpcodes )
    {
      case Event( Tcp.Received( incomingDataByteString ) , stateData : BotService.ParsingIncomingOpcodesStateData ) ⇒
      {
        if( incomingDataByteString.length == 0 )
          stay using stateData
        else
          stop( FSM.Failure( new RuntimeException( "In this state we should definitely not receive any tcp incoming data (but we received '" + incomingDataByteString.utf8String + "')" ) ) )
      }

      case Event( StateTimeout , stateData ) ⇒
      {
        stop( FSM.Failure( new RuntimeException( "Timeout while parsing incoming opcode" ) ) )
      }

      case Event( IncomingOpcodesParser.Parsed( incomingOpcodeEither ) , stateData : BotService.ParsingIncomingOpcodesStateData ) ⇒
      {
        cancelTimer( BotService.ParsingIncomingOpcodes.getClass.getSimpleName )

        incomingOpcodeEither match
        {
          case scala.Right( incomingOpcode ) ⇒
          {
            stateData.remotelyDeployedBotActor ! incomingOpcode

            setTimer( BotService.SerializingOutgoingOpcodes.getClass.getSimpleName , StateTimeout , stateData.botServiceConfig.preparingOutgoingOpcodesByAkkaBotTimeoutInSeconds second , false )

            goto( BotService.SerializingOutgoingOpcodes ) using new BotService.SerializingOutgoingOpcodesStateData( stateData.botServiceConfig , stateData.akkaSocketActorManager , stateData.incomingDataBuffer , stateData.remotelyDeployedBotActor , stateData.incomingOpcodesParser , stateData.outgoingOpcodesSerializer )
          }

          case scala.Left( throwable ) ⇒ stop( FSM.Failure( new RuntimeException( "Problem while parsing incoming opcode " + throwable , throwable ) ) )
        }
      }
    }

    when( BotService.SerializingOutgoingOpcodes )
    {
      case Event( StateTimeout , stateData ) ⇒
      {
        stop( FSM.Failure( new RuntimeException( "Timeout while waiting for akka-bot response (while preparing response)" ) ) )
      }

      case Event( outgoingOpcodesAnonSeq : Seq[_] , stateData : BotService.SerializingOutgoingOpcodesStateData ) ⇒
      {
        cancelTimer( BotService.SerializingOutgoingOpcodes.getClass.getSimpleName )

        try
        {
          val outgoingOpcodesSeq = outgoingOpcodesAnonSeq.asInstanceOf[ Seq[ OutgoingOpcode ] ]

          // TODO maybe analyze outgoing seq to monitor if it has duplicates of opcodes and log it

          stateData.outgoingOpcodesSerializer ! new OutgoingOpcodesSerializer.Serialize( outgoingOpcodesSeq )

          setTimer( BotService.SendingOutgoingOpcodes.getClass.getSimpleName , StateTimeout , stateData.botServiceConfig.serializingOutgoingOpcodesTimeoutInSeconds second , false )

          goto( BotService.SendingOutgoingOpcodes ) using new BotService.SendingOutgoingOpcodesStateData( stateData.botServiceConfig , stateData.akkaSocketActorManager , stateData.incomingDataBuffer , stateData.remotelyDeployedBotActor , stateData.incomingOpcodesParser , stateData.outgoingOpcodesSerializer )
        }
        catch
        {
          case throwable : Throwable =>  stop( FSM.Failure( new RuntimeException( "Problem with received outgoing opcodes seq, " + throwable , throwable ) ) )
        }
      }
    }

    when( BotService.SendingOutgoingOpcodes )
    {
      case Event( StateTimeout , stateData ) ⇒
      {
        stop( FSM.Failure( new RuntimeException( "Timeout while serializing outgoing opcodes" ) ) )
      }

      case Event( OutgoingOpcodesSerializer.Serialized( outgoingOpcodesSeqAsByteStringEither ) , stateData : BotService.SendingOutgoingOpcodesStateData ) ⇒
      {
        cancelTimer( BotService.SendingOutgoingOpcodes.getClass.getSimpleName )

        outgoingOpcodesSeqAsByteStringEither match
        {
          case scala.Right( outgoingOpcodesSeqAsByteString ) ⇒
          {
            log.debug( "serialized outgoing opcodes seq '" + outgoingOpcodesSeqAsByteString.utf8String + "'" )

            val baos = new java.io.ByteArrayOutputStream()
            val dos = new java.io.DataOutputStream( baos )
            dos.writeInt( outgoingOpcodesSeqAsByteString.length )
            dos.flush

            stateData.akkaSocketActorManager ! new akka.io.Tcp.Write( ByteString( baos.toByteArray ) ++ outgoingOpcodesSeqAsByteString , akka.io.Tcp.NoAck )

            gotoToReadCommandLengthState( new BotService.ReadingCommandLengthStateData( stateData.botServiceConfig , stateData.akkaSocketActorManager , stateData.incomingDataBuffer , stateData.remotelyDeployedBotActor , stateData.incomingOpcodesParser , stateData.outgoingOpcodesSerializer ) )
          }

          case scala.Left( throwable ) ⇒ stop( FSM.Failure( new RuntimeException( "Problem while serializing outgoing opcodes " + throwable , throwable ) ) )
        }
      }
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( BotService.Stop( throwable ) , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received Stop message, throwable '" + throwable + "' with state data " + stateData , throwable ) ) )

      case Event( Tcp.CommandFailed( command ) , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Could not read command, failed command '" + command + "'" ) ) )

      case Event( connectionClosed : Tcp.ConnectionClosed , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Could not read command, connection closed with '" + connectionClosed + "'" ) ) )

      case Event( unknownMessage , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" ) ) )
    }

    onTermination
    {
      case StopEvent(stopType, state, stateData) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stopping (normal), state '" + state + "', state data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stopping (shutdown), state '" + state + "', state data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stopping (failure = " + cause + "), state '" + state + "', state data " + stateData )
        }

        if ( stateData.isInstanceOf[ BotService.InitializedStateData ] )
        {
          val initializedStateData = stateData.asInstanceOf[ BotService.InitializedStateData ]

          // just to be nice for server.io we close socket by sending Tcp.Close
          // (it is not needed since we are 'listener actor' and socket will be closed when this actor stop)
          initializedStateData.akkaSocketActorManager ! Tcp.Close

          initializedStateData.remotelyDeployedBotActor ! PoisonPill
        }
      }
    }

    initialize

    // ----

    protected def gotoToReadCommandLengthState( stateData : BotService.ReadingCommandLengthStateData ) = gotoToTcpIpReadingCommandLengthOrBodyState( BotService.ReadingCommandLength , stateData )

    protected def gotoToReadCommandBodyState( stateData : BotService.ReadingCommandBodyStateData ) = gotoToTcpIpReadingCommandLengthOrBodyState( BotService.ReadingCommandBody , stateData )

    protected def gotoToTcpIpReadingCommandLengthOrBodyState( state : BotService.State , stateData : BotService.StateData ) =
    {
      // very important do send empty string to wake up state even if there were no new incoming data (but maybe there are previously collected data, it have to be checked)
      self ! new Tcp.Received( ByteString("") )
      goto( state ) using stateData
    }

    // ----
  }
}
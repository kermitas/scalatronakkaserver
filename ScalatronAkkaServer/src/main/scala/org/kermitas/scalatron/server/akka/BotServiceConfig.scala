package main.scala.org.kermitas.scalatron.server.akka
{
  // ==========================

  object BotServiceConfig
  {
    def apply( typesafeConfig : com.typesafe.config.Config ) : BotServiceConfig =
    {
      val readingCommandLengthTimeoutInSeconds = typesafeConfig.getInt( "readingCommandLengthTimeoutInSeconds" )
      val readingCommandBodyTimeoutInSeconds = typesafeConfig.getInt( "readingCommandBodyTimeoutInSeconds" )
      val parsingIncomingOpcodesTimeoutInSeconds = typesafeConfig.getInt( "parsingIncomingOpcodesTimeoutInSeconds" )
      val preparingOutgoingOpcodesByAkkaBotTimeoutInSeconds = typesafeConfig.getInt( "preparingOutgoingOpcodesByAkkaBotTimeoutInSeconds" )
      val serializingOutgoingOpcodesTimeoutInSeconds = typesafeConfig.getInt( "serializingOutgoingOpcodesTimeoutInSeconds" )

      val mappings = IncomingConnectionSourceAddressToDestinationAkkaAddressMapping( typesafeConfig.getConfigList( classOf[ IncomingConnectionSourceAddressToDestinationAkkaAddressMapping ].getSimpleName ) )

      new BotServiceConfig( readingCommandLengthTimeoutInSeconds , readingCommandBodyTimeoutInSeconds , parsingIncomingOpcodesTimeoutInSeconds , preparingOutgoingOpcodesByAkkaBotTimeoutInSeconds , serializingOutgoingOpcodesTimeoutInSeconds , mappings )
    }
  }

  case class BotServiceConfig( readingCommandLengthTimeoutInSeconds : Int , readingCommandBodyTimeoutInSeconds : Int , parsingIncomingOpcodesTimeoutInSeconds : Int , preparingOutgoingOpcodesByAkkaBotTimeoutInSeconds : Int , serializingOutgoingOpcodesTimeoutInSeconds : Int , mappings : Array[ IncomingConnectionSourceAddressToDestinationAkkaAddressMapping ] )

  // ==========================

  object IncomingConnectionSourceAddressToDestinationAkkaAddressMapping
  {
    def apply( typesafeConfigs : java.util.List[ _ <: com.typesafe.config.Config] ) : Array[ IncomingConnectionSourceAddressToDestinationAkkaAddressMapping ] =
    {
      val result = new scala.collection.mutable.ListBuffer[ IncomingConnectionSourceAddressToDestinationAkkaAddressMapping ]

      val iterator = typesafeConfigs.iterator

      while( iterator.hasNext ) result += readSingleIncomingConnectionSourceAddressToDestinationAkkaAddressMapping( iterator.next )

      result.toArray
    }

    protected def readSingleIncomingConnectionSourceAddressToDestinationAkkaAddressMapping( typesafeConfig : com.typesafe.config.Config ) : IncomingConnectionSourceAddressToDestinationAkkaAddressMapping =
    {
      val sourceIp = typesafeConfig.getString( "sourceIp" )
      val destinationAkkaBotAddress = typesafeConfig.getString( "destinationAkkaBotAddress" )

      new IncomingConnectionSourceAddressToDestinationAkkaAddressMapping( sourceIp , destinationAkkaBotAddress )
    }
  }

  case class IncomingConnectionSourceAddressToDestinationAkkaAddressMapping( sourceIp : String , destinationAkkaBotAddress : String )

  // ==========================
}


package main.scala.org.kermitas.scalatron.server.akka
{
  import akka.actor._
  import akka.event._
  import akka.util._
  import main.scala.org.kermitas.scalatron.opcodes._

  object IncomingOpcodesParser
  {
    sealed trait Messages

      sealed trait IncomingMessages extends Messages
        case class Parse( incomingOpcodeAsByteString : ByteString ) extends IncomingMessages

      sealed trait OutgoingMessages extends Messages
        case class Parsed( incomingOpcodeSeqEither : Either[ Throwable , IncomingOpcode ] ) extends OutgoingMessages
  }

  class IncomingOpcodesParser extends Actor
  {
    protected val log = Logging(context.system, this)

    def receive =
    {
      case IncomingOpcodesParser.Parse( incomingOpcodeAsByteString ) ⇒
      {
        sender ! new IncomingOpcodesParser.Parsed( parseIncomingOpcodes( incomingOpcodeAsByteString ) )
      }

      case m ⇒ log.warning( "Received unknown message " + m )
    }

    protected def parseIncomingOpcodes( incomingOpcodeAsByteString : ByteString ) : Either[ Throwable , IncomingOpcode ] =
    {
      log.debug( "Parsing incoming opcode (ByteString) '" + incomingOpcodeAsByteString.utf8String + "'" )

      try
      {
        val incomingOpcodeAndNewByteStringTuple =
        {
          if ( incomingOpcodeAsByteString.startsWith( "Welcome" ) )
            parseWelcomeOpcode( incomingOpcodeAsByteString )
          else
          if ( incomingOpcodeAsByteString.startsWith( "React" ) )
            parseReactOpcode( incomingOpcodeAsByteString )
          else
          if ( incomingOpcodeAsByteString.startsWith( "Goodbye" ) )
            parseGoodbyeOpcode( incomingOpcodeAsByteString )
          else
            throw new RuntimeException( "Unrecognized opcode in beginning of string '" + incomingOpcodeAsByteString.utf8String + "'" )
        }

        if ( incomingOpcodeAndNewByteStringTuple._2.length != 0 ) throw new RuntimeException( "Not whole incoming ByteString was parsed (after parsing opcode there is still '" + incomingOpcodeAndNewByteStringTuple._2.utf8String + "' inside)" )

        scala.Right( incomingOpcodeAndNewByteStringTuple._1 )
      }
      catch
      {
        case throwable : Throwable ⇒
        {
          log.debug( "Caught throwable " + throwable , throwable )
          scala.Left( throwable )
        }
      }
    }

    // ----

    protected def parseWelcomeOpcode( incomingOpcodeAsByteString : ByteString ) : ( IncomingOpcode , ByteString ) =
    {
      var analyzedByteString = incomingOpcodeAsByteString.drop( 7 ) // Welcome

      val mapAndNewByteStringTuple = parseParameters( analyzedByteString )

      val map = mapAndNewByteStringTuple._1
      analyzedByteString = mapAndNewByteStringTuple._2

      val name = getAndRemoveRequiredStringParameter( "name" , map )
      val apocalypse = getAndRemoveRequiredIntParameter( "apocalypse" , map )
      val round = getAndRemoveRequiredIntParameter( "round" , map )
      val maxSlavesOption : Option[Int] = if ( map.contains( "maxSlaves" ) ) Some( getAndRemoveRequiredIntParameter( "maxSlaves" , map ) ) else None

      val welcomeOpcode = new Welcome( name=name , apocalypse=apocalypse , round=round , maxSlavesOption=maxSlavesOption )

      ( welcomeOpcode , analyzedByteString )
    }

    protected def parseReactOpcode( incomingOpcodeAsByteString : ByteString ) : ( IncomingOpcode , ByteString ) =
    {
      var analyzedByteString = incomingOpcodeAsByteString.drop( 5 ) // React

      val mapAndNewByteStringTuple = parseParameters( analyzedByteString )

      val map = mapAndNewByteStringTuple._1
      analyzedByteString = mapAndNewByteStringTuple._2

      val generation = getAndRemoveRequiredIntParameter( "generation" , map )
      val name = getAndRemoveRequiredStringParameter( "name" , map )
      val time = getAndRemoveRequiredIntParameter( "time" , map )
      val view = getAndRemoveRequiredViewParameter( "view" , map )
      val energy = getAndRemoveRequiredIntParameter( "energy" , map )
      val collisionOption : Option[Direction] = if ( map.contains( "collision" ) ) Some( getAndRemoveRequiredDirectionParameter( "collision" , map ) ) else None
      val slavesOption : Option[Int] = if ( map.contains( "slaves" ) ) Some( getAndRemoveRequiredIntParameter( "slaves" , map ) ) else None

      val reactOpcode : IncomingOpcode =
      {
        if ( map.contains( "master" ) )
        {
          val master = getAndRemoveRequiredPointParameter( "master" , map )
          new MiniBotReact( generation=generation , name=name , time=time , view=view , energy=energy , master=master , collisionOption=collisionOption , slavesOption=slavesOption , customProperties=map )
        }
        else
        {
          new MasterBotReact( generation=generation , name=name , time=time , view=view , energy=energy , collisionOption=collisionOption , slavesOption=slavesOption , customProperties=map )
        }
      }

      ( reactOpcode , analyzedByteString )
    }

    protected def parseGoodbyeOpcode( incomingOpcodeAsByteString : ByteString ) : ( IncomingOpcode , ByteString ) =
    {
      var analyzedByteString = incomingOpcodeAsByteString.drop( 7 ) // Goodbye

      val mapAndNewByteStringTuple = parseParameters( analyzedByteString )

      val map = mapAndNewByteStringTuple._1
      analyzedByteString = mapAndNewByteStringTuple._2

      val energy = getAndRemoveRequiredIntParameter( "energy" , map )

      val goodbyeOpcode = new Goodbye( energy=energy , customProperties=map )

      ( goodbyeOpcode , analyzedByteString )
    }

    // ----

    protected def parseParameters( incomingOpcodeAsByteString : ByteString ) : ( scala.collection.mutable.Map[String,String] , ByteString ) =
    {
      var analyzedByteString = incomingOpcodeAsByteString.drop(1) // (

      val str = analyzedByteString.takeWhile( _ != ')' )
      analyzedByteString = analyzedByteString.drop( str.length + 1 ) // drop last ')"
      if ( analyzedByteString.startsWith( "|" ) ) analyzedByteString = analyzedByteString.drop( 1 )  // drop last '|"

      ( parsePropertiesMap( str ) , analyzedByteString )
    }

    protected def parsePropertiesMap( inputString : ByteString ) : scala.collection.mutable.Map[String,String] =
    {
      val map = new scala.collection.mutable.HashMap[String,String]()

      val array = inputString.utf8String.split( "," )

      for ( kv <- array )
      {
        val arr = kv.split("=")
        map += ( arr(0) -> arr(1) )
      }

      map
    }

    // ----

    protected def getAndRemoveRequiredIntParameter( parameterName : String , map : scala.collection.mutable.Map[String,String] ) : Int = Integer.parseInt( getAndRemoveRequiredStringParameter( parameterName , map ) )

    protected def getAndRemoveRequiredStringParameter( parameterName : String , map : scala.collection.mutable.Map[String,String] ) : String =
    {
      map.remove( parameterName ) match
      {
        case Some( value ) ⇒ value.asInstanceOf[String]
        case None ⇒ throw new RuntimeException( "Could not find '" + parameterName + "' parameter" )
      }
    }

    protected def getAndRemoveRequiredPointParameter( parameterName : String , map : scala.collection.mutable.Map[String,String] ) : java.awt.Point =
    {
      val array = getAndRemoveRequiredStringParameter( parameterName , map ).split( ":" )
      val i1 = Integer.parseInt( array(0) )
      val i2 = Integer.parseInt( array(1) )

      new java.awt.Point( i1 , i2 )
    }

    protected def getAndRemoveRequiredDirectionParameter( parameterName : String , map : scala.collection.mutable.Map[String,String] ) : Direction =
    {
      val point = getAndRemoveRequiredPointParameter( parameterName , map )

      Direction.getDirection( point.x , point.y ) match
      {
        case Some( direction ) => direction
        case None => throw new RuntimeException( "Could not create direction parameter using '" + point.x + ":" + point.y + "'" )
      }
    }

    protected def getAndRemoveRequiredViewParameter( parameterName : String , map : scala.collection.mutable.Map[String,String] ) : View = View( getAndRemoveRequiredStringParameter( parameterName , map ) )
  }
}
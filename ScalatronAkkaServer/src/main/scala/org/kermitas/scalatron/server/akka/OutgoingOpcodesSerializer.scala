package main.scala.org.kermitas.scalatron.server.akka
{
  import akka.actor._
  import akka.event._
  import akka.util._
  import main.scala.org.kermitas.scalatron.opcodes._

  object OutgoingOpcodesSerializer
  {
    sealed trait Messages

      sealed trait IncomingMessages extends Messages
        case class Serialize( outgoingOpcodesSeq : Seq[ OutgoingOpcode ] ) extends IncomingMessages

      sealed trait OutgoingMessages extends Messages
        case class Serialized( outgoingOpcodesSeqAsByteStringEither : Either[ Throwable , ByteString ] ) extends OutgoingMessages
  }

  class OutgoingOpcodesSerializer extends Actor
  {
    protected val log = Logging(context.system, this)

    def receive =
    {
      case OutgoingOpcodesSerializer.Serialize( outgoingOpcodesSeq ) ⇒
      {
        sender ! new OutgoingOpcodesSerializer.Serialized( serializeOutgoingOpcodes( outgoingOpcodesSeq ) )
      }

      case m ⇒ log.warning( "Received unknown message " + m )
    }

    protected def serializeOutgoingOpcodes( outgoingOpcodesSeq : Seq[ OutgoingOpcode ] ) : Either[ Throwable , ByteString ] =
    {
      log.debug( "Serializing outgoing opcodes (Seq) '" + outgoingOpcodesSeq + "'" )

      try
      {
        var bs = new StringBuilder

        val it = outgoingOpcodesSeq.iterator
        while( it.hasNext )
        {
          val outgoingOpcode = it.next
          serializeOutgoingOpcode( outgoingOpcode , bs )
          if ( it.hasNext ) bs.append( "|" )
        }

        scala.Right( ByteString( bs.toString ) )
      }
      catch
      {
        case throwable : Throwable ⇒
        {
          log.debug( "Caught throwable " + throwable , throwable )
          scala.Left( throwable )
        }
      }
    }

    // ----

    protected def serializeOutgoingOpcode( outgoingOpcode : OutgoingOpcode , sb : StringBuilder)
    {
      var internalProperties = new scala.collection.mutable.HashMap[ String , Any ]

      outgoingOpcode match
      {
        case Move( direction , customPropertiesOption ) =>
        {
          internalProperties += ( "direction" -> direction )

          serializeOutgoingOpcode( "Move" , internalProperties , customPropertiesOption , sb )
        }

        case Spawn( direction , name , energy , customPropertiesOption ) =>
        {
          if ( energy < 100 ) throw new RuntimeException( "Energy should be minimum 100" )

          internalProperties += ( "name" -> name )
          internalProperties += ( "energy" -> energy )
          internalProperties += ( "direction" -> direction )

          serializeOutgoingOpcode( "Spawn" , internalProperties , customPropertiesOption , sb )
        }

        case Set( customProperties ) =>
        {
          serializeOutgoingOpcode( "Set" , internalProperties , Some( customProperties ) , sb )
        }

        case Explode( size ) =>
        {
          if ( size < 3 || size > 9 ) throw new RuntimeException( "Size should be not less than 3 and not bigger 9" )

          internalProperties += ( "size" -> size )

          serializeOutgoingOpcode( "Explode" , internalProperties , None , sb )
        }

        case Say( text ) =>
        {
          checkText( text , 10 )

          internalProperties += ( "text" -> text )

          serializeOutgoingOpcode( "Say" , internalProperties , None , sb )
        }

        case main.scala.org.kermitas.scalatron.opcodes.Status( text ) =>
        {
          checkText( text , 20 )

          internalProperties += ( "text" -> text )

          serializeOutgoingOpcode( "Status" , internalProperties , None , sb )
        }

        case Log( text ) =>
        {
          checkText( text , Int.MaxValue )

          internalProperties += ( "text" -> text )

          serializeOutgoingOpcode( "Log" , internalProperties , None , sb )
        }

        case MarkCell( position , color ) =>
        {
          internalProperties += ( "position" -> position )
          internalProperties += ( "color" -> color )

          serializeOutgoingOpcode( "MarkCell" , internalProperties , None , sb )
        }

        case DrawLine( from , to , color ) =>
        {
          internalProperties += ( "from" -> from )
          internalProperties += ( "to" -> to )
          internalProperties += ( "color" -> color )

          serializeOutgoingOpcode( "DrawLine" , internalProperties , None , sb )
        }
      }
    }

    protected def checkText( text : String , maxLength : Int )
    {
      if( text.length > maxLength ) throw new IllegalArgumentException( "Text length should be maximum " + maxLength + " characters long (now it has " + text.length + " in '" + text + "')" )
      if ( text.contains( "," ) || text.contains( "|" ) || text.contains( "=" ) || text.contains( "(" ) || text.contains( ")" ) ) throw new IllegalArgumentException( "Text cannot contains ,|=()" )
    }

    protected def serializeOutgoingOpcode( outgoingOpcodeName : String , internalProperties : scala.collection.Map[String,Any] , customPropertiesOption : Option[ scala.collection.Map[ String , String ] ] , sb : StringBuilder)
    {
      validateCustomProperties( customPropertiesOption )

      sb.append( outgoingOpcodeName ).append( "(" )

      serializeMap( internalProperties , "" , sb )

      customPropertiesOption match
      {
        case Some( customProperties ) => serializeMap( customProperties , if(internalProperties.size>0) "," else "" , sb )
        case None =>
      }

      sb.append( ")" )
    }

    protected def validateCustomProperties( customPropertiesOption : Option[ scala.collection.Map[ String , String ] ] )
    {
      customPropertiesOption match
      {
        case Some( customProperties ) =>
        {
          def validateIfNotContains( keyName : String ) = if( customProperties.contains( keyName )  ) throw new RuntimeException( "Custom properties cannot contains '" + keyName + "' key" )

          validateIfNotContains( "generation" )
          validateIfNotContains( "name" )
          validateIfNotContains( "energy" )
          validateIfNotContains( "time" )
          validateIfNotContains( "view" )
          validateIfNotContains( "direction" )
          validateIfNotContains( "master" )
          validateIfNotContains( "collision" )
          validateIfNotContains( "slaves" )
        }

        case None =>
      }
    }

    protected def serializeMap( map : scala.collection.Map[String,Any] , ifMapIsNotEmptyUseThisPrefix : String , sb : StringBuilder )
    {
      val it = map.iterator

      if ( it.hasNext ) sb.append( ifMapIsNotEmptyUseThisPrefix )

      while ( it.hasNext )
      {
        val kv = it.next

        val serializedValue = kv._2 match
        {
          case point : java.awt.Point => point.x + ":" + point.y

          case Direction( horizontalDirection , verticalDirection ) => horizontalDirection.getDirectionAsInt + ":" + verticalDirection.getDirectionAsInt

          case other => other.toString
        }

        sb.append( kv._1 + "=" + serializedValue )
        if ( it.hasNext ) sb.append( "," )
      }
    }
  }
}
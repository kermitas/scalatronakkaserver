package main.scala.org.kermitas.scalatron.server.akka
{
  import akka.actor._
  import akka.event._
  import akka.io._
  import scala.concurrent.duration._

  object ScalatronAkkaServer
  {
    // ----

    sealed trait State
      case object Uninitialized extends State
      case object Working extends State

    // ----

    sealed trait StateData
      case object UninitializedStateData extends StateData
      case object WorkingStateData extends StateData

    // ----

    sealed trait Messages

      sealed trait IncomingMessages extends Messages

        case class Start( scalatronAkkaServerConfig : ScalatronAkkaServerConfig ) extends IncomingMessages
        case class Stop( throwable : Throwable ) extends IncomingMessages

      sealed trait OutgoingMessages extends Messages

        case class StartStatus( throwableOption : Option[Throwable] ) extends OutgoingMessages

    // ----
  }

  class ScalatronAkkaServer extends Actor with FSM[ScalatronAkkaServer.State, ScalatronAkkaServer.StateData]
  {
    startWith( ScalatronAkkaServer.Uninitialized , ScalatronAkkaServer.UninitializedStateData )

    when( ScalatronAkkaServer.Uninitialized )
    {
      case Event( ScalatronAkkaServer.Start( scalatronAkkaServerConfig ) , ScalatronAkkaServer.UninitializedStateData ) ⇒
      {
        try
        {
          startKeyListener( scalatronAkkaServerConfig.checkIfUserPressedAnyKeyIntervalInMilliSeconds )

          val incomingOpcodesParser = context.actorOf( Props[IncomingOpcodesParser] , name = classOf[ IncomingOpcodesParser ].getSimpleName )
          val outgoingOpcodesSerializer = context.actorOf( Props[OutgoingOpcodesSerializer] , name = classOf[ OutgoingOpcodesSerializer ].getSimpleName )

          doBindings( scalatronAkkaServerConfig.bindings , incomingOpcodesParser , outgoingOpcodesSerializer )

          sender ! ScalatronAkkaServer.StartStatus( None )

          goto( ScalatronAkkaServer.Working ) using ScalatronAkkaServer.WorkingStateData
        }
        catch
        {
          case t : Throwable ⇒
          {
            sender ! ScalatronAkkaServer.StartStatus( Some( t ) )
            stop( FSM.Failure( t ) )
          }
        }
      }
    }

    when( ScalatronAkkaServer.Working )
    {
      // just a dummy event because this state is completely empty
      case Event( StateTimeout , stateData ) ⇒ stay using stateData
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( ScalatronAkkaServer.Stop( throwable ) , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received Stop message, throwable '" + throwable + "' with state data " + stateData , throwable ) ) )
      case Event( unknownMessage , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" ) ) )
    }

    onTermination
    {
      case StopEvent(stopType, state, stateData) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stoping (normal), state '" + state + "', state data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stoping (shutdown), state '" + state + "', state data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stoping (failure = " + cause + "), state '" + state + "', state data " + stateData )
        }

        new _root_.main.scala.org.kermitas.scalatron.util.JVMKillingThread( 4 , false ).start

        context.system.shutdown
      }
    }

    initialize

    // ----

    protected def startKeyListener( checkIfUserPressedAnyKeyIntervalInMilliSeconds : Int )
    {
      import main.scala.org.kermitas.scalatron.util._
      val keyPressedListenerActor = context.actorOf( Props[ KeyPressedListenerActor ] , name = classOf[ KeyPressedListenerActor ].getSimpleName )
      keyPressedListenerActor ! new KeyPressedListenerActor.Init( self , new ScalatronAkkaServer.Stop( new Throwable( "User requested stop" ) ) , checkIfUserPressedAnyKeyIntervalInMilliSeconds )
      log.info( "Press enter to shutdown" )
    }

    protected def doBindings( bindingsConfig : Array[ Binding ] , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef )
    {
      val serverSocketHandlerIncrementalId = new java.util.concurrent.atomic.AtomicInteger

      for( bindingConfig <- bindingsConfig ) doSingleBinding( bindingConfig , serverSocketHandlerIncrementalId , incomingOpcodesParser , outgoingOpcodesSerializer )
    }

    protected def doSingleBinding( bindingConfig : Binding , serverSocketHandlerIncrementalId : java.util.concurrent.atomic.AtomicInteger , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef )
    {
      val serverSocketHandler = context.actorOf( Props[ ServerSocketHandler ] , name = classOf[ ServerSocketHandler ].getSimpleName + "-" + serverSocketHandlerIncrementalId )
      serverSocketHandlerIncrementalId.incrementAndGet

      serverSocketHandler ! new ServerSocketHandler.Init( bindingConfig , incomingOpcodesParser , outgoingOpcodesSerializer )
    }
  }
}

package main.scala.org.kermitas.scalatron.server.akka
{
  // ==========================

  object ScalatronAkkaServerConfig
  {
    def apply( typesafeConfig : com.typesafe.config.Config ) : ScalatronAkkaServerConfig =
    {
      var tc = typesafeConfig

      val logLevel = java.util.logging.Level.parse( tc.getString( "logLevel" ) )
      val workAsAkkaRemoteDeploymentNode = tc.getBoolean( "workAsAkkaRemoteDeploymentNode" )

      val actorSystemName = tc.getString( "actorSystemName" )

      tc = tc.getConfig( "AkkaNormalMode" )

      val checkIfUserPressedAnyKeyIntervalInMilliSeconds = tc.getInt("checkIfUserPressedAnyKeyIntervalInMilliSeconds")

      val mainActorExecuteInitialMessageTimeoutInSeconds = tc.getInt("mainActorExecuteInitialMessageTimeoutInSeconds")

      val bindings = Binding( tc.getConfigList( classOf[ Binding ].getSimpleName ) )

      new ScalatronAkkaServerConfig( logLevel , workAsAkkaRemoteDeploymentNode , actorSystemName , checkIfUserPressedAnyKeyIntervalInMilliSeconds , mainActorExecuteInitialMessageTimeoutInSeconds , bindings )
    }
  }

  case class ScalatronAkkaServerConfig( logLevel : java.util.logging.Level , workAsAkkaRemoteDeploymentNode : Boolean , actorSystemName : String , checkIfUserPressedAnyKeyIntervalInMilliSeconds : Int , mainActorExecuteInitialMessageTimeoutInSeconds : Int , bindings : Array[ Binding ] )

  // ==========================

  object Binding
  {
    def apply( typesafeConfigs : java.util.List[ _ <: com.typesafe.config.Config ] ) : Array[ Binding ] =
    {
      val result = new scala.collection.mutable.ListBuffer[ Binding ]

      val iterator = typesafeConfigs.iterator

      while( iterator.hasNext ) result += readSingleBinding( iterator.next )

      result.toArray
    }

    protected def readSingleBinding( typesafeConfig : com.typesafe.config.Config ) : Binding =
    {
      val bindingTimeoutInSeconds = typesafeConfig.getInt( "bindingTimeoutInSeconds" )

      val localListenAddresses = LocalListenAddress( typesafeConfig.getConfigList( classOf[ LocalListenAddress ].getSimpleName ) )

      val botServiceConfig = BotServiceConfig( typesafeConfig.getConfig( classOf[ BotService ].getSimpleName ) )

      new Binding( bindingTimeoutInSeconds , localListenAddresses , botServiceConfig )
    }
  }

  case class Binding( bindingTimeoutInSeconds : Int , localListenAddresses : Array[ LocalListenAddress ] , botServiceConfig : BotServiceConfig )

  // ==========================

  object LocalListenAddress
  {
    def apply( typesafeConfigs : java.util.List[ _ <: com.typesafe.config.Config ] ) : Array[ LocalListenAddress ] =
    {
      val result = new scala.collection.mutable.ListBuffer[ LocalListenAddress ]

      val iterator = typesafeConfigs.iterator

      while( iterator.hasNext ) result += readSingleLocalListenAddress( iterator.next )

      result.toArray
    }

    protected def readSingleLocalListenAddress( typesafeConfig : com.typesafe.config.Config ) : LocalListenAddress =
    {
      val ip = typesafeConfig.getString( "ip" )
      val port = typesafeConfig.getInt( "port" )
      val stopOnBindFailure = typesafeConfig.getBoolean( "stopOnBindFailure" )

      new LocalListenAddress( ip , port , stopOnBindFailure )
    }
  }

  case class LocalListenAddress( ip : String , port : Int , stopOnBindFailure : Boolean )

// ==========================
}

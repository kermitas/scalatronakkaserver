package main.scala.org.kermitas.scalatron.server.akka
{
  import akka.actor._
  import akka.event._
  import akka.io._
  import scala.concurrent.duration._

  object ServerSocketHandler
  {
    // ----

    sealed trait State
      case object Uninitialized extends State
      case object Binding extends State
      case object Bound extends State

    // ----

    sealed trait StateData
    case object UninitializedStateData extends StateData
    class InitializedStateData( bindingConfig : Binding , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef ) extends StateData
      case class BindingStateData( bindingConfig : Binding , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef , var localListenAddressArrayIndex : Int ) extends InitializedStateData( bindingConfig , incomingOpcodesParser , outgoingOpcodesSerializer )
      case class BoundStateData( bindingConfig : Binding , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef , var botServiceIncrementalId : Int ) extends InitializedStateData( bindingConfig , incomingOpcodesParser , outgoingOpcodesSerializer )

    // ----

    sealed trait Messages

      sealed trait IncomingMessages extends Messages

        case class Init( bindingConfig : Binding , incomingOpcodesParser : ActorRef , outgoingOpcodesSerializer : ActorRef ) extends IncomingMessages
        case class Stop( throwable : Throwable ) extends IncomingMessages

    // ----
  }

  class ServerSocketHandler extends Actor with FSM[ServerSocketHandler.State, ServerSocketHandler.StateData]
  {
    // TODO Configure: if child actor will blow that should not hurt us (current actor)

    // ----

    startWith( ServerSocketHandler.Uninitialized , ServerSocketHandler.UninitializedStateData )

    when( ServerSocketHandler.Uninitialized )
    {
      case Event( ServerSocketHandler.Init( bindingConfig , incomingOpcodesParser , outgoingOpcodesSerializer ) , ServerSocketHandler.UninitializedStateData ) ⇒
      {
        if( bindingConfig.localListenAddresses.size > 0 )
        {
          setTimer( ServerSocketHandler.Binding.getClass.getSimpleName , StateTimeout , bindingConfig.bindingTimeoutInSeconds second , false )

          doSingleBinding( bindingConfig.localListenAddresses( 0 ) )
          goto( ServerSocketHandler.Binding ) using new ServerSocketHandler.BindingStateData( bindingConfig , incomingOpcodesParser , outgoingOpcodesSerializer , 0 )
        }
        else
          goto( ServerSocketHandler.Bound ) using new ServerSocketHandler.BoundStateData( bindingConfig , incomingOpcodesParser , outgoingOpcodesSerializer , 0 )
      }
    }

    when( ServerSocketHandler.Binding )
    {
      case Event( StateTimeout , stateData : ServerSocketHandler.BindingStateData ) ⇒
      {
        stop( FSM.Failure( new RuntimeException( "Timeout while making bindings" ) ) )
      }

      case Event( Tcp.CommandFailed( command ) , stateData : ServerSocketHandler.BindingStateData ) ⇒
      {
        if( stateData.bindingConfig.localListenAddresses( stateData.localListenAddressArrayIndex ).stopOnBindFailure )
          stop( FSM.Failure( new RuntimeException( "Could not bind, failed command '" + command + "'" ) ) )
        else
          continueBindingOrGoToBundState( stateData )
      }

      case Event( bound : Tcp.Bound , stateData : ServerSocketHandler.BindingStateData ) ⇒
      {
        continueBindingOrGoToBundState( stateData )
      }
    }

    when( ServerSocketHandler.Bound )
    {
      case Event( connection : Tcp.Connected , stateData : ServerSocketHandler.BoundStateData ) ⇒
      {
        log.info( "New client (nr. " + stateData.botServiceIncrementalId + ") accepted " + connection )

        val botService = context.actorOf( Props[ BotService ] , name = classOf[ BotService ].getSimpleName + "-" + stateData.botServiceIncrementalId )
        stateData.botServiceIncrementalId += 1

        botService ! BotService.Init( stateData.bindingConfig.botServiceConfig , sender , connection.remoteAddress.getAddress.getHostAddress , stateData.incomingOpcodesParser , stateData.outgoingOpcodesSerializer )

        sender ! Tcp.Register( botService )

        stay using stateData
      }
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( ServerSocketHandler.Stop( throwable ) , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received Stop message, throwable '" + throwable + "' with state data " + stateData , throwable ) ) )
      case Event( unknownMessage , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" ) ) )
    }

    onTermination
    {
      case StopEvent(stopType, state, stateData) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stoping (normal), state '" + state + "', state data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stoping (shutdown), state '" + state + "', state data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stoping (failure = " + cause + "), state '" + state + "', state data " + stateData )
        }

        // TODO how to send to akka Tcp manager requests to close owned by me server sockets???

        context.system.actorFor( "/user/" + classOf[ ScalatronAkkaServer ].getSimpleName ) ! new ScalatronAkkaServer.Stop( new Throwable( "Actor '" + self + "' stopped, (stop type '" + stopType + "')" ) )
      }
    }

    initialize

    // ----

    protected def continueBindingOrGoToBundState( stateData : ServerSocketHandler.BindingStateData ) =
    {
      if( stateData.bindingConfig.localListenAddresses.size -1 == stateData.localListenAddressArrayIndex )
      {
        cancelTimer( ServerSocketHandler.Binding.getClass.getSimpleName )

        goto( ServerSocketHandler.Bound ) using new ServerSocketHandler.BoundStateData( stateData.bindingConfig , stateData.incomingOpcodesParser , stateData.outgoingOpcodesSerializer , 0 )
      }
      else
      {
        stateData.localListenAddressArrayIndex += 1
        doSingleBinding( stateData.bindingConfig.localListenAddresses( stateData.localListenAddressArrayIndex ) )

        stay using stateData
      }
    }

    protected def doSingleBinding( localListenAddress : LocalListenAddress )
    {
      val localAddress = new java.net.InetSocketAddress( localListenAddress.ip , localListenAddress.port )

      log.info( "Binding to " + localAddress )

      val tcpManager = akka.io.IO( Tcp )( context.system )
      tcpManager ! akka.io.Tcp.Bind( self , localAddress )
    }
  }
}